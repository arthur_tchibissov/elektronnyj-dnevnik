# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#from users.models import User
from django.contrib.auth.models import User
# Create your models here.

class Announcement(models.Model):
    title = models.CharField(max_length = 150)
    body = models.TextField()
    author = models.ForeignKey(User, on_delete = models.DO_NOTHING)
    date_pub = models.DateTimeField(auto_now_add = True)

    #def __str__(self):
        #return u'{}'.format(self.title)
