"""project10 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views
#form django.urls import path

urlpatterns = [
    url(r'^$', views.index, name = 'home'),
    url(r'^add$', views.add, name = 'home'),
    url(r'^delete$', views.delete, name = 'home'),
    url(r'^open_edit$', views.open_edit, name = 'home'),
    url(r'^edit$', views.edit, name = 'home'),
]
