# -*- coding: utf-8 -*-
from django import forms
from models import schForm

class UpdateUserForm(forms.Form):
    surname = forms.CharField(label = 'Фамилия', widget = forms.TextInput(attrs={'size': 60}))
    firstname = forms.CharField(label = 'Имя', widget = forms.TextInput(attrs={'size': 60}))
    #patronymic = forms.CharField(label = 'Отчество', widget = forms.TextInput(attrs={'size': 60}))
    #sex = forms.TypedChoiceField(choices = [("", "Не указан"), ("m", "М"), ("f", "Ж")], label = 'Пол')
    #phone_number = forms.CharField(label = 'Телефон', widget = forms.TextInput(attrs={'size': 60}))
    email = forms.EmailField(label = 'Email', widget = forms.TextInput(attrs={'size': 60}))
    username = forms.CharField(label = 'Имя пользователя', widget = forms.TextInput(attrs={'size': 60}))
    #password = forms.CharField(label = 'Пароль', widget = forms.TextInput(attrs={'size': 60}))
    #date_birth = forms.DateField(label = 'Дата рождения')

class CreateUserForm(UpdateUserForm):
    #surname = forms.CharField(label = 'Фамилия', widget = forms.TextInput(attrs={'size': 60}))
    #firstname = forms.CharField(label = 'Имя', widget = forms.TextInput(attrs={'size': 60}))
    #patronymic = forms.CharField(label = 'Отчество', widget = forms.TextInput(attrs={'size': 60}))
    #sex = forms.TypedChoiceField(choices = [("", "Не указан"), ("m", "М"), ("f", "Ж")], label = 'Пол')
    #phone_number = forms.CharField(label = 'Телефон', widget = forms.TextInput(attrs={'size': 60}))
    #email = forms.EmailField(label = 'Email', widget = forms.TextInput(attrs={'size': 60}))
    #username = forms.CharField(label = 'Имя пользователя', widget = forms.TextInput(attrs={'size': 60}))
    password = forms.CharField(label = 'Пароль', widget = forms.TextInput(attrs={'size': 60}))
    #date_birth = forms.DateField(label = 'Дата рождения')

class chForm(forms.Form):
    form = forms.ModelChoiceField(label = 'Класс', queryset = schForm.objects.all(), empty_label = None, to_field_name = 'id', widget = forms.Select())

class edit_staff(CreateUserForm):
    pass

class importForm(forms.Form):
    file = forms.FileField()
