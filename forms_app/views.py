# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from users.models import schForm

from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required
def index(request):
    title = 'Классы'
    forms = schForm.objects.all()
    q = forms.count()
    return render(request, "forms_app/index.html", {"title": title, "forms": forms, 'q': q})
