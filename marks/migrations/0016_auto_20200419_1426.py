# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-04-19 14:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20200325_1519'),
        ('marks', '0015_lesson_hometask'),
    ]

    operations = [
        migrations.CreateModel(
            name='Absence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(blank=True, max_length=2, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='lesson',
            name='hometask',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AlterField(
            model_name='lesson',
            name='topic',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AddField(
            model_name='absence',
            name='lesson',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='marks.Lesson'),
        ),
        migrations.AddField(
            model_name='absence',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='users.Student'),
        ),
    ]
