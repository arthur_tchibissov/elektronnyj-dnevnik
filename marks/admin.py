# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from . models import Period, Subject, Markbook, Lesson, Work, WorkMark, WorkType, Result, Absence

class WorkAdmin(admin.ModelAdmin):
    list_display = ['id', 'lesson', 'type']

class MarkbookAdmin(admin.ModelAdmin):
    list_display = ['id', 'form', 'subject', 'period', 'teacher']

class HometaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'lesson', 'title']

class HometaskMarkAdmin(admin.ModelAdmin):
    list_display = ['id', 'hometask', 'student', 'mark']

class WorkMarkAdmin(admin.ModelAdmin):
    list_display = ['id', 'work', 'student', 'mark']

class WorkTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'weight']

class ResultAdmin(admin.ModelAdmin):
    list_display = ['id', 'markbook', 'student', 'mark']

admin.site.register(Period)
admin.site.register(Subject)
admin.site.register(Markbook, MarkbookAdmin)
admin.site.register(Lesson)
# admin.site.register(Hometask, HometaskAdmin)
# admin.site.register(HometaskMark, HometaskMarkAdmin)
admin.site.register(WorkType, WorkTypeAdmin)
admin.site.register(Work, WorkAdmin)
admin.site.register(WorkMark, WorkMarkAdmin)
admin.site.register(Result, ResultAdmin)
admin.site.register(Absence)
