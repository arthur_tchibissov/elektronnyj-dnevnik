# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from users.models import schForm, Staff, Student

# Create your models here.
class Period(models.Model):
    title = models.CharField(max_length = 150)
    begin = models.DateField()
    end = models.DateField()

    class Meta:
        verbose_name = (u'Период')
        verbose_name_plural = (u'Периоды')

    def __unicode__(self):
        return u"%s" % self.title

    def __str__(self):
        return self.__unicode__()

class Subject(models.Model):
    title = models.CharField(max_length = 150)

    class Meta:
        verbose_name = (u'Предмет')
        verbose_name_plural = (u'Предметы')

    def __unicode__(self):
        return u"%s" % self.title

    def __str__(self):
        return self.__unicode__()

class Markbook(models.Model):
    form = models.ForeignKey(schForm, on_delete = models.DO_NOTHING)
    subject = models.ForeignKey(Subject, on_delete = models.DO_NOTHING)
    period = models.ForeignKey(Period, on_delete = models.DO_NOTHING)
    teacher = models.ForeignKey(Staff, on_delete = models.DO_NOTHING)

    class Meta:
        verbose_name = (u'Журнал')
        verbose_name_plural = (u'Журналы')

    def __unicode__(self):
        return u"%s" % (str(self.form.year) + "-" + str(self.form.number) + " " + self.subject.title + " " + self.period.title)

    def __str__(self):
        return self.__unicode__()

class Lesson(models.Model):
    markbook = models.ForeignKey(Markbook, on_delete = models.DO_NOTHING)
    date = models.DateField()
    topic = models.CharField(max_length = 150, blank = True)
    hometask = models.CharField(max_length = 150, blank = True)
    num = models.IntegerField(blank = True, null = True)
    class Meta:
        verbose_name = (u'Урок')
        verbose_name_plural = (u'Уроки')

    def __unicode__(self):
        return u"%s" % self.markbook + " --- " + str(self.date) + "(" + str(self.num) + ")"

    def __str__(self):
        return self.__unicode__()

# class Hometask(models.Model):
#     lesson = models.ForeignKey(Lesson, on_delete = models.DO_NOTHING)
#     title = models.CharField(max_length = 150)
#     class Meta:
#         verbose_name = (u'Домашнее задание')
#         verbose_name_plural = (u'Домашние задания')
#     def __unicode__(self):
#         return u"%s" % self.lesson + " --- " + self.title
#     def __str__(self):
#         return self.__unicode__()

# class HometaskMark(models.Model):
#     hometask = models.ForeignKey(Hometask, on_delete = models.DO_NOTHING)
#     student = models.ForeignKey(Student, on_delete = models.DO_NOTHING)
#     mark = models.IntegerField(blank = True, null = True)
#     class Meta:
#         verbose_name = (u'Оценка за д/з')
#         verbose_name_plural = (u'Оценки за д/з')
#     def __unicode__(self):
#         return u"%s" % self.hometask + " --- " + unicode(self.student) + " --- " + str(self.mark)
#     def __str__(self):
#         return self.__unicode__()

class WorkType(models.Model):
    title = models.CharField(max_length = 150)
    titleSh = models.CharField(max_length = 2)
    weight = models.FloatField()
    class Meta:
        verbose_name = (u'Тип работ')
        verbose_name_plural = (u'Типы работ')
    def __unicode__(self):
        return u"%s" % self.title + " (" + str(self.weight) + ")"
    def __str__(self):
        return self.__unicode__()

class Work(models.Model):
    lesson = models.ForeignKey(Lesson, on_delete = models.DO_NOTHING)
    type = models.ForeignKey(WorkType, on_delete = models.DO_NOTHING)
    class Meta:
        verbose_name = (u'Работа')
        verbose_name_plural = (u'Работы')
    def __unicode__(self):
        return u"%s" % self.lesson + " --- " + unicode(self.type)
    def __str__(self):
        return self.__unicode__()

class WorkMark(models.Model):
    work = models.ForeignKey(Work, on_delete = models.DO_NOTHING)
    student = models.ForeignKey(Student, on_delete = models.DO_NOTHING)
    mark = models.IntegerField(blank = True, null = True)
    class Meta:
        verbose_name = (u'Оценка за работу')
        verbose_name_plural = (u'Оценки за работы')
    def __unicode__(self):
        return u"%s" % self.work + " --- " + unicode(self.student) + " --- " + str(self.mark)
    def __str__(self):
        return self.__unicode__()

class Result(models.Model):
    markbook = models.ForeignKey(Markbook, on_delete = models.DO_NOTHING)
    student = models.ForeignKey(Student, on_delete = models.DO_NOTHING)
    mark = models.CharField(max_length = 3)
    class Meta:
        verbose_name = (u'Итоговая оценка')
        verbose_name_plural = (u'Итоговые оценки')
    def __unicode__(self):
        return u"%s" % self.markbook + " --- " + unicode(self.student) + " --- " + unicode(self.mark)
    def __str__(self):
        return self.__unicode__()

class Absence(models.Model):
    student = models.ForeignKey(Student, on_delete = models.DO_NOTHING)
    lesson = models.ForeignKey(Lesson, on_delete = models.DO_NOTHING)
    value = models.CharField(max_length = 2, blank = True, null = True)
